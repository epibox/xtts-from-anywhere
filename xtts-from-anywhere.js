// ==UserScript==
// @name         XTTS from Anywhere!
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  try to take over the world!
// @author       EpiBox
// @match        https://app.novelcrafter.com/*
// @grant        none
// ==/UserScript==

(function() {
    var clickState = 0;
    var speakers = [];

    document.addEventListener('mouseup', async function(event) {
        console.log("Global " + clickState + speakers.length)

        if (speakers.length <= 0) {
            console.log("Pulling the speaker list");
            var response = await fetch('http://localhost:8020/speakers_list');
            speakers = await response.json();
            console.log(speakers);
            console.log ("done pulling" + speakers.length)
        }

        function createSpeakerSelect(event, speakers) {
            const select = document.createElement('select');
            select.style.position = 'absolute';
            select.style.left = (event.pageX + 30) + 'px';
            select.style.top = event.pageY + 'px';

            speakers.forEach(speaker => {
                const option = document.createElement('option');
                option.value = speaker;
                option.textContent = speaker;
                select.appendChild(option);
            });

            return select;
        }

        const speakerSelect = createSpeakerSelect(event, speakers);

        var selection = window.getSelection().toString();

        if (selection.length <= 0){
            clickState = 0
            console.log("Resetting clickState to " + clickState)
        }

        if (selection.length > 0 && clickState == 0) {
            clickState = 1;
            console.log("setting clickstate " + clickState);
            var playButton = document.createElement('playButton');
            playButton.textContent = 'Play';
            playButton.style.position = 'absolute';
            playButton.style.left = event.pageX + 'px';
            playButton.style.top = event.pageY + 'px';
            playButton.style.backgroundColor = 'lightgrey';

            var closeButton = document.createElement('closeButton');
            closeButton.textContent = 'Close';
            closeButton.style.position = 'absolute';
            closeButton.style.left = event.pageX + 'px';
            closeButton.style.top = (event.pageY + 20) + 'px';
            closeButton.style.backgroundColor = 'lightgrey';

            var voiceSelect = document.createElement('voiceSelect');
            voiceSelect.style.position = 'absolute';
            voiceSelect.style.left = (event.pageX + 180) + 'px';
            voiceSelect.style.top = event.pageY + 'px';

            speakerSelect.addEventListener('click', function(){
              console.log(speakerSelect.value)
            })

            const option = document.createElement('option');
            const select = document.createElement('select');

            function createSpeakerSelect(event, speakers) {

                select.style.position = 'absolute';
                select.style.left = (event.pageX + 20) + 'px';
                select.style.top = event.pageY + 'px';

                speakers.forEach(speaker => {
                    option.value = speaker;
                    option.textContent = speaker;
                    select.appendChild(option);
                });

                return select;
            }

            function closeAll (){
                clickState = 0;
                window.getSelection().removeAllRanges();
                closeButton.remove();
                playButton.remove();
                voiceSelect.remove();
                speakerSelect.remove();
            }

            playButton.addEventListener('click', function() {
                var xhr = new XMLHttpRequest();
                xhr.open('POST', 'http://localhost:8020/tts_to_audio');
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.setRequestHeader('Content-Type', 'application/json');
                xhr.send(JSON.stringify({
                    text: selection,
                    speaker_wav: speakerSelect.value,
                    language: 'en'
                }));

                closeAll();
            });

            closeButton.addEventListener('mouseup', function() {
                closeAll();
            });

            document.body.appendChild(speakerSelect);
            document.body.appendChild(playButton);
            document.body.appendChild(closeButton);
            document.body.appendChild(voiceSelect);
        }
  });
})();
