## XTTS on Any Site!
This tool will allow you to use a locally running instance of the XTTS api server to generate audio from any text you select on a website. It was created with the intent of being used with the writing tool NovelCrafter.

## Installation
To install and use this tool, you'll need to have a browser that supports TamperMonkey. More information on TamperMonkey can be found here [tampermonkey](https://www.tampermonkey.net/)

You also need the following installed:
- python3.10
- xtts-api-server by [daswer](https://github.com/daswer123)
- ffmpeg

## Usage
Before the tool can work, the xtts-api-server must be running, and the script must be enabled. 

To fully benefit from the script, it is best to use the following prompt to start it up:
> python -m xtts_api_server --deepspeed --streaming-mode --stream-play-sync

Once these requirements are met, simply select the text you want to have read back and click play! You can also select a different voice from the drop down, or simply 'Close' the menu.

## Known Issues
- Menu stays on even when you click away. WORKAROUND: Use the 'Close' button to remove it
- Can't stop the audio from playing. No workaround at this time.

## Roadmap
- Adjustments to the UI so it's more visible
- Add a 'Play' button for reply
- Add ability to download the audio files
- Addition of play.ht support

## Contributing
If you wish to contribute, be sure to properly comment your code additions.

## Authors and acknowledgment
This work could not be done without the hard work that daswer already put into making the xtts projects.
