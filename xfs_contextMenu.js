// ==UserScript==
// @name         XFA Context Menu
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  try to take over the world!
// @author       EpiBox
// @match        https://app.novelcrafter.com/*
// @grant        GM_registerMenuCommand
// @grant        GM.xmlHttpRequest
// ==/UserScript==

(async function() {
    // GLOBAL VARS
    var clickState = 0;
    var speakers = [];
    var speakerSelect;
    var selection;

    if (speakers.length <= 0) {
        console.log("Pulling the speaker list");
        var response = await fetch('http://localhost:8020/speakers_list');
        speakers = await response.json();
        console.log(speakers);
        console.log ("done pulling" + speakers.length)
    }

    function tester(voice){
     console.log(voice);
    }

    // FUNCTIONS
    function voiceSynthLocal(voice){
        if (selection.length >= 100){
        console.debug("Synthesizing!")
        var xhr = new XMLHttpRequest();
            xhr.open('POST', 'http://localhost:8020/tts_to_audio');
            xhr.setRequestHeader('Accept', 'application/json');
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.send(JSON.stringify({
                text: selection,
                speaker_wav: voice,
                language: 'en'
            }));
        }
    }

    document.addEventListener('mouseup', function(event) {
        if (event.button == 0){
            selection = window.getSelection().toString();
            console.debug(selection.length)
            if(selection.length >= 100){
                console.debug("Ready for sync!")
            } else {
                console.debug("Selection is too short...")
            }
        }

        if (event.button == 1){
            voiceSynthLocal(selection);
        }
    });

    speakers.forEach(speaker => {
        GM_registerMenuCommand(speaker, () => voiceSynthLocal(speaker));
    });
})();
